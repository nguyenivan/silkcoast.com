#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util,template
from google.appengine.api import users

class MainHandler(webapp.RequestHandler):
    def get(self):
        self.redirect('/index.html')


def main():
    application = webapp.WSGIApplication([	('/', MainHandler)
										], debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
