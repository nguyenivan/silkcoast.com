﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util,template
from google.appengine.api import users
from models import *

class MainHandler(webapp.RequestHandler):
    def get(self):
        self.redirect('/index.html')

class InitHandler(webapp.RequestHandler):
	def get(self):
		key_name_list = ['home_project', 'home_intro', 'home_contact', 'projects', 'intro', 'share', 'contact']
		for keyName in key_name_list:
			content = Content(key_name = keyName, Html = keyName)
			content.put()
			
		self.response.out.write("Tạo ra các content ban đầu. Done!")
		
class UploadHandler(webapp.RequestHandler):
    
	def render (self, message):
		images = Image.all(keys_only=True).fetch(20)
		if os.environ.get('HTTP_HOST'):
			url = os.environ['HTTP_HOST']
		else:
			url = os.environ['SERVER_NAME']
		urls = dict([(image.name(),"http://%s/upload?name=%s" % (url,image.name()) ) for image in images])
		template_values = {	"urls":urls,
							"message": message
							}
							
		
		path = 'upload.tpl'
		self.response.out.write(template.render(path,template_values))
		
	def get(self):
		name = self.request.get("name")
		if not name:
			message = "Please upload file < 1MB"
			self.render(message)
		else:
			# Serve image
			image = Image.get_by_key_name(name)
			if image is None:
				raise Exception("Image name does not exist")
			self.response.headers['Content-Type'] = "image/png"
			self.response.out.write(image.File)
	
	def post(self):
		# Upload image
		delete_list = self.request.get_all("delete")
		#self.response.out.write(delete_list)
		#return
		message = ''
		if delete_list:
			images = Image.get_by_key_name(delete_list)
			count = 0
			for image in images:
				image.delete()
				count += 1
			message = "%d image deleted." % count
				
		file = self.request.get("image")
		if file:
			images = Image.all(keys_only=True).fetch(offset=20, limit=1)
			if images:
				# File count exceeded
				message += "Cannot store more than 20 files."
			else:
				filename = self.request.POST["image"].filename
				image = Image(key_name = filename, File = db.Blob(file))
				image.put()
				message += "Image stored successfully."
		self.render(message)
			
class HomeHandler(webapp.RequestHandler):
	def get(self):
		home_project = Content.get_by_key_name('home_project')
		home_intro = Content.get_by_key_name('home_intro')
		home_contact = Content.get_by_key_name('home_contact')
		is_admin = users.is_current_user_admin()
		template_values = {	'is_admin' : is_admin,
							'home_project' : home_project.Html,
							'home_intro' : home_intro.Html,
							'home_contact' : home_contact.Html,
							'logout_url': users.create_logout_url("/")
						}
		path = 'index.tpl'
		self.response.out.write(template.render(path,template_values))
		
class IntroHandler(webapp.RequestHandler):
	def get(self):
		content = Content.get_by_key_name('intro')
		is_admin = users.is_current_user_admin()
		template_values = {	'is_admin' : is_admin,
							'intro' : content.Html
						}
		path = 'intro.tpl'
		self.response.out.write(template.render(path,template_values))

class ProjectsHandler(webapp.RequestHandler):
	def get(self):
		content = Content.get_by_key_name('projects')
		is_admin = users.is_current_user_admin()
		template_values = {	'is_admin' : is_admin,
							'projects' : content.Html
						}
		path = 'projects.tpl'
		self.response.out.write(template.render(path,template_values))
		
class ShareHandler(webapp.RequestHandler):
	def get(self):
		content = Content.get_by_key_name('share')
		is_admin = users.is_current_user_admin()
		template_values = {	'is_admin' : is_admin,
							'share' : content.Html
						}
		path = 'share.tpl'
		self.response.out.write(template.render(path,template_values))
		
class ContactHandler(webapp.RequestHandler):
	def get(self):
		content = Content.get_by_key_name('contact')
		is_admin = users.is_current_user_admin()
		template_values = {	'is_admin' : is_admin,
							'contact' : content.Html
						}
		path = 'contact.tpl'
		self.response.out.write(template.render(path,template_values))
		
class AuthorHandler(webapp.RequestHandler):
	def get(self):
		key_name_list = ['home_project', 'home_intro', 'home_contact', 'projects', 'intro', 'share', 'contact']
		key_name = self.request.get('key_name')
		need_logout = users.get_current_user() and not users.is_current_user_admin()
		logout_url = ''
		if need_logout:
			logout_url = users.create_logout_url('/author')
		if key_name.strip() == '':
			template_values = { 'need_logout': need_logout,
								'logout_url': logout_url,
								'home' : users.create_login_url('/')}
			path = 'pages.tpl'
			self.response.out.write(template.render(path,template_values))
		else:
			content = Content.get_by_key_name(key_name)
			if content is None:
				raise Exception('Cannot query content with key name: %s' % key_name)
			
			template_values = {	'content' : content.Html,
								'key_name' : key_name,
								'referer' : self.request.headers.get("Referer")
								}
			path = 'author.tpl'
			self.response.out.write(template.render(path,template_values))
	
	def post(self):
		# Update content from data store
		key_name = self.request.get('key_name')
		html = self.request.get('content')
		referer = self.request.get('referer')
		if html.strip() =='':
			raise Exception ('Will not update empty content!')
		content = Content.get_by_key_name(key_name)
		if content is None:
			raise Exception('Cannot query content with key name: %s' % key_name)
		content.Html = html
		content.put()
		self.redirect(referer)
		

def main():
    application = webapp.WSGIApplication([	('/', MainHandler),
											('/init',InitHandler),
											('/upload',UploadHandler),
											('/author',AuthorHandler),
											('/index.html',HomeHandler),
											('/intro.html', IntroHandler),
											('/projects.html', ProjectsHandler),
											('/share.html', ShareHandler),
											('/contact.html', ContactHandler)
										],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
